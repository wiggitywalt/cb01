<cfset setEnabled(true)>
<cfset setUniqueURLs(true)>

<cfif len(getSetting("AppMapping")) lte 1>
	<cfset setBaseURL("http://#cgi.HTTP_HOST#/index.cfm")>
<cfelse>
	<cfset setBaseURL("http://#cgi.HTTP_HOST#/#getSetting('AppMapping')#/index.cfm")>
</cfif>

<!--- CUSTOM COURSES GO HERE (they will be checked in order) --->
<cfset addRoute(pattern="entry/:id",handler="general",action="viewPost")>

<!--- STANDARD COLDBOX COURSES, DO NOT MODIFY UNLESS YOU DON'T LIKE THEM --->
<cfset addRoute(":handler/:action?/:id?")>